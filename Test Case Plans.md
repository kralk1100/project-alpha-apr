# Test Case Plans
## Login:
* Description: User is able to log in to their account
* Priority: High
* Acceptance Criteria: The user can reach the login page, enter their credentials, and click the login button. User is then redirected to the My Projects page.

## Create a Project link
* Description: User is able to begin creating a project
* Priority: Medium
* Acceptance Criteria: User can reach the form page to enter their project details from the link.

## Logout:
* Description: User is able to log out of their account
* Priority: High
* Acceptance Criteria: User can log out of their account from the navbar log out button.

## User Stories
### Login:
* As a user, I want to be able to log in to my account in order to create projects and tasks.

### Create a Project link:
* As a user, I want to be able to create projects to add to my account

### Logout:
* As a user, I want to be able to log out of my account.

## Scope
* Login
* Create a Project link
* Logout

## Test Environments
### Operating system
* Windows 10

### Browser
* Google Chrome 114

## Tools
* Selenium webdriver
* Snipping Tool
* VSCode

# Summary
![testresults](/uploads/3ac5afb58e2c4685de41d45479272674/testresults.JPG)
