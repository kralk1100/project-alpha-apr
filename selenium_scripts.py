from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
import config


class LoginPage:
    def __init__(self, driver):
        self.driver = driver
        self.username_input = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, "id_username"))
        )
        self.password_input = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, "id_password"))
        )

    def login(self, username, password):
        self.username_input.clear()
        self.username_input.send_keys(username)
        self.password_input.clear()
        self.password_input.send_keys(password)
        self.password_input.submit()


class CreateProjectPage:
    def __init__(self, driver):
        self.driver = driver
        self.create_project_link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(
                (By.XPATH, "//a[@href='/projects/create/']")
            )
        )

    def open_create_project(self):
        self.create_project_link.click()


class LogoutPage:
    def __init__(self, driver):
        self.driver = driver
        self.logout_link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(
                (By.XPATH, "//a[@href='/accounts/logout/']")
            )
        )

    def logout(self):
        self.logout_link.click()


chrome_options = Options()
driver = webdriver.Chrome(options=chrome_options)
driver.get("http://127.0.0.1:8000/accounts/login/")

login_page = LoginPage(driver)
login_page.login(config.user_key, config.pass_key)

assert "My Projects" in driver.page_source, "Error: Login failed"
print("Successfully logged in")

create_project_page = CreateProjectPage(driver)
create_project_page.open_create_project()

assert (
    "Description" in driver.page_source
), "Error: Failed to open Create a Project link"
print("Successfully opened the Create a Project link")


logout_page = LogoutPage(driver)
logout_page.logout()

assert "Login" in driver.page_source, "Error: Logout failed"
print("Successfully logged out")

driver.quit()
