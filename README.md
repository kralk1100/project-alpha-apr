# Task Tracker
* Developed project and task management functionality utilizing Django and Python for the back-end, while incorporating HTML and CSS for the front-end, including the creation of project models, task models, and signup/login/logout forms.
* Optimized code structure and readability with djlint and flake8 tools to ensure high standards of cleanliness and quality
* Utiliized Selenium with Python to automate tests for logging in, creating a project link, and logging out. Scripts are found in selenium_scripts.py and test case plans are found in Test Case Plans.md

![tasks_login](/uploads/a34e9b03c28c029f491dc98dff8d9ab7/tasks_login.JPG)
![tasks_page](/uploads/6f0cca207f807c17e489e1e97a80aac0/tasks_page.JPG)
![tasks_tasks](/uploads/8b871907c3d2fbbad98037c95c5efea5/tasks_tasks.JPG)
![tasks_pass](/uploads/3a1478305a1a2672c314f8cd6cc54215/tasks_pass.JPG)
